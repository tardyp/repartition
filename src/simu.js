export const levels = ["cp", "ce1", "ce2", "cm1", "cm2"];

function random_pick(arr) {
    return arr[Math.floor(Math.random() * arr.length)];
}
function randint(max) {
    return Math.floor(Math.random() * (max + 1))
}
function deepcopy(classes){
    var new_classes = []
    classes.forEach((x) =>
        new_classes.push(JSON.parse(JSON.stringify(x))))
    return new_classes
}
var num_schools = 5;
var result_schools = 1;
var num_variants = 100;
var num_switches = 10;
var num_iterations = 100;
const student_pick = 27;
var previous_state = null;
function add_level(cls, level, num){
    var found = false
    for (var l of cls.levels){
        if (l.name === level) {
            l.num += num
            if (l.num < 0) {
                num -= l.num
            }
            found = true
        }
    }
    cls.levels = cls.levels.filter((l) => l.num > 0)
    if (!found){
        if (cls.levels.length > 2)
            throw new Error("too many levels")
        cls.levels.push({
            name: level,
            num: num
        })
        function compare_levels(a, b) {
            function calc_level(l){
                return levels.indexOf(l.name)
            }
            return calc_level(a) - calc_level(b)
        }
        cls.levels.sort(compare_levels)
    }
    if (cls.levels.length === 0)
        throw new Error("not enough levels")
    cls.num_student += num
    var total = 0
    for (l of cls.levels){
        total += l.num
    }
    if (total !== cls.num_student)
    debugger;    
    return -num;
}
export function runSimu({
        num_classrooms,
        num_multilevels,
        minimal_group,
        target_double,
        num_multilevels_weight,
        split_weight,
        num_student_weight, ...extra }) {
    var num_students={}
    var target_students={}
    var split_students={}
    var total = 0
    levels.forEach((level) => {
        num_students[level] = extra["num_" + level]
        target_students[level] = extra["target_" + level]
        split_students[level] = extra["split_" + level]
        total += extra["num_" + level]
    })
    var base_student = Math.ceil(total / num_classrooms)
    function compare_levels(a, b) {
        function calc_level(cls){
            var base = levels.indexOf(cls.levels[0].name)
            if (cls.levels.length >1)
                base += 0.5
            return base
        }
        return calc_level(a) - calc_level(b)
    }
    function gen_initial_classes() {
        var classes = []
        var remaining_students = {};
        levels.forEach((level) => {
            remaining_students[level] = num_students[level]
        })
        function is_remaining_students(levels) {
            var total = 0
            levels.forEach((level) => {
                total += remaining_students[level]
            })
            return total > 0
        }
        for (var i = 0; i < num_classrooms; i++) {
            var clas = {
                num_student: 0,
                levels: []
            }
            var candidate_levels = levels;
            while (clas.num_student < base_student && is_remaining_students(candidate_levels)) {
                var level = random_pick(candidate_levels)
                if (remaining_students[level] <= 0)
                    continue
                var needed_student = base_student - clas.num_student
                var picked_students = needed_student>student_pick? student_pick: needed_student
                if (remaining_students[level] < picked_students) {
                    picked_students = remaining_students[level]
                }
                add_level(clas, level, picked_students)
                remaining_students[level] -= picked_students
                var leveli = levels.indexOf(level)
                if (leveli + 1 < levels.length)
                    candidate_levels = [level, levels[leveli + 1]]
                else if(remaining_students[level]){
                    candidate_levels = [level]
                } else
                    break

            }
            classes.push(clas)
        }
        if (is_remaining_students(levels)) {
            return null;
        }
        classes.sort(compare_levels)
        return classes;
    }
    function gen_incremental_classes(classes) {
        var new_classes = deepcopy(classes)
        var level
        try {
            for(var i = 0; i < num_switches; i++){
                var cls = randint(classes.length - 2)
                var num = randint(1) + 1
                var dir = randint(1)
                var cls1 = new_classes[cls]
                var cls2 = new_classes[cls + 1]
                if (dir){ /* move students up */
                    level = cls1.levels[cls1.levels.length - 1].name
                    num = add_level(cls1, level, -num)
                    add_level(cls2, level, num)
                } else { /* move students down */
                    level = cls2.levels[0].name
                    num = add_level(cls2, level, -num)
                    add_level(cls1, level, num)
                }
                new_classes.sort(compare_levels)
            }
        }
        catch {
            /* in case of impossible, we just bail out and return initial classe, because the class is corrupted */
             return classes
        }
    // return arr[ * arr.length)];
        return new_classes
    }
    function calc_fitness(classes) {
        var fitness = 0;
        var num_multilevel = 0
        for(var clas of classes){
            if (clas.levels.length === 0){
                return {fitness: 10000000}
            }
            var target_num = target_students[clas.levels[0].name ];
            if (clas.levels.length>1)
                target_num = target_double
            fitness += num_student_weight * Math.abs(target_num - clas.num_student)

            if (clas.levels.length > 1)
                num_multilevel ++
            if (clas.levels.length > 2)
                fitness += 10000
        }
        if (num_multilevel > num_multilevels)
            fitness += 10000
        for (var level of levels) {
            var split = split_students[level]
            if (split > 0) {
                var num_split = 0
                for(clas of classes){
                    for(var l of clas.levels){
                        if (l.name === level){
                            num_split += 1
                        }
                        if (l.num < minimal_group)
                            fitness += 100000
                    }
                }
                if (num_split !== split) {
                    fitness += split_weight*split_weight
                }
            }

        }
            
        fitness += num_multilevels_weight * num_multilevel
        return {
            fitness: fitness,
            school: classes
        }
    }
    function compare_fitness(a, b) {
        return a.fitness - b.fitness
    }
    function generate_initial_state() {
        var schools = []
        for (var i = 0; i < num_schools; i++) {
            var classes = null;
            for(var j = 0; j< 1000; j ++){
                try {
                classes = gen_initial_classes()
                } catch {
                    classes = null
                }
                if (classes != null)
                    break
            }
            if (classes == null)
                return {error:"impossible de creer l'etat initial"}
            schools.push(classes)
        }
        return schools;
    }
    var schools = previous_state;
    if (schools == null) {
        schools = generate_initial_state()
    }
    for (var iter = 0; iter < num_iterations; iter++) {
        var next_schools = []
        for (var school of schools) {
            next_schools.push(school)
            for (var i = 0; i < num_variants; i++) {
                next_schools.push(gen_incremental_classes(school))
            }
        }
        next_schools = next_schools.map(calc_fitness)
        next_schools.sort(compare_fitness)
        schools = []
        for (i = 0; i < num_schools; i++) {
            schools.push(next_schools[i].school)
        }
    }
    function format_classes(classes) {
        classes = calc_fitness(classes)
        classes.school = classes.school.map((cls) =>
        ({
            name: cls.levels.map((v) => v.name).join("-") + "(" + cls.num_student + ")",
            desc: cls.levels.map((v) => v.num + " " + v.name).join(", ")
        }))
        return classes
    }
    previous_state = schools.slice(0,result_schools)
    return previous_state.map(format_classes)
}