import React, { useState } from 'react';
import { Button } from 'antd';
import { Row, Col } from 'antd';
import { InputNumber } from 'antd';
import { Form, Table,Slider } from 'antd';
import { runSimu, levels } from "./simu.js"
import './App.css';
import LZString from "lz-string"

function json_to_b64( values ) {
  return  LZString.compressToBase64(JSON.stringify(values));
}

function b64_to_json( str ) {
  var v = LZString.decompressFromBase64(str )
  console.log(v)
  return JSON.parse(v);
}

const App = () => {
  var [results, setResults] = useState([])
  function onFinish(values) {
    window.location.hash = json_to_b64(values)
    window.localStorage['initials'] = JSON.stringify(values)
    setResults(runSimu(values))
  }
  var initials = window.location.hash
  try {
      initials = b64_to_json(initials.slice(1))
    }
    catch (e){
        initials = undefined
    }
  if (!initials) {
    try {
      initials = JSON.parse(window.localStorage['initials'])
    }
    catch (e){
        initials = {}
    }
  }
  const columns = [
    {
      title: 'Classe',
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: 'Eleves',
      dataIndex: 'desc',
      key: 'name',
    },]
    return (
    <div className="App">
      <Row >
        <Col span={24}>
          <Form
            initialValues={initials}
            name="basic"
            labelCol={{ span: 8 }}
            wrapperCol={{ span: 16 }}
            onFinish={onFinish}
          // onFinishFailed={onFinishFailed}
          >
          <Row gutter={[8, 8]}>
          <Col span={6}>
            <h2>Paramètres d'école:</h2>
            <Form.Item
              label="Nombre de classes"
              name="num_classrooms"
              rules={[{ required: true }]}
            >
              <InputNumber />
            </Form.Item>
            {levels.map((value, index) => {
              return (
                <Form.Item
                  label={"Nombre de " + value}
                  name={"num_" + value}
                  rules={[{ required: true }]}
                  key={index}
                >
                  <InputNumber />
                </Form.Item>
              )
            })}
          </Col>
          <Col span={6}>
            <h3>Nombre d'élèves ideal par classe:</h3>
            <div>
            {levels.map((value, index) => {
              return (
                <Form.Item
                  label={value}
                  name={"target_" + value}
                  rules={[{ required: true }]}
                  key={index}
                >
                  <InputNumber />
                </Form.Item>
              )
            })}
            </div>
            <Form.Item
              label="double niveau"
              name="target_double"
              rules={[{ required: true }]}
            >
              <InputNumber />
            </Form.Item>
            </Col>
            <Col span={6}>
            <h3>Séparation de groupe ideal:</h3>
            <div>
            {levels.map((value, index) => {
              return (
                <Form.Item
                  label={value}
                  name={"split_" + value}
                  rules={[{ required: false }]}
                  key={index}
                >
                  <InputNumber />
                </Form.Item>
              )
            })}
            </div>
            <h3>Nombre minimum par groupe:</h3>
            <Form.Item
              label="de niveau"
              name="minimal_group"
              rules={[{ required: true }]}
            >
              <InputNumber />
            </Form.Item>
            </Col>
            <Col span={6}>
            <h2>Critères d'optimisation:</h2>
            <h3>Optimiser le nombre d'enfant par classe:</h3>
            <Form.Item
              label="importance"
              name="num_student_weight"
              rules={[{ required: true }]}
            >
              <Slider min={1} max={100} tooltipVisible={false} />
            </Form.Item>
            <h3>Nombre de double niveaux:</h3>
            <Form.Item
              label="Nombre max"
              name="num_multilevels"
              rules={[{ required: true }]}
            >
              <InputNumber />
            </Form.Item>
            <Form.Item
              label="importance"
              name="num_multilevels_weight"
              rules={[{ required: true }]}
            >
              <Slider min={1} max={100} tooltipVisible={false} />
            </Form.Item>
            <h3>Séparation de groupe:</h3>
            <Form.Item
              label="importance"
              name="split_weight"
              rules={[{ required: true }]}
            >
              <Slider min={1} max={100} tooltipVisible={false} />
            </Form.Item>
            <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
              <Button type="primary" htmlType="submit">
                Optimiser
              </Button>
            </Form.Item>
            </Col>
            </Row>
          </Form>
        </Col>
        </Row>
        <Row>
          {results.error}
          {(results.length)?
            <div>
              <h2>Resultats</h2>
              <div>
              {results.map((value, index) => 
                <Table
                size="small"
                key={index}
                dataSource={value.school}
                columns={columns}
                pagination={false}
                />
              )}</div>
            </div>:<div/>
          }
      </Row>
    </div>
  );
}

export default App;